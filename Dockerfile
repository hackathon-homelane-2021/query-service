FROM mhart/alpine-node:12 as builder

WORKDIR /app

COPY . /app

RUN	apk add --no-cache git=2.20.4-r0 && \
	npm i --no-audit --production

FROM alpine:3.7

COPY --from=builder /usr/bin/node /usr/bin/
COPY --from=builder /usr/lib/libgcc* /usr/lib/libstdc* /usr/lib/
COPY --from=builder /app /app

WORKDIR /app

RUN apk add --no-cache tini=0.16.1-r0 curl=7.61.1-r3

ENV PORT=4444 \
	NODE_ENV=production \
	LOG_LEVEL=debug \
	MAX_EVENT_LOOP_DELAY=1000 \
	MAX_RSS_BYTES=0 \
	MAX_HEAP_USED_BYTES=0 \
	MAX_AGE=86400

EXPOSE $PORT

# an init entrypoint that simplifies signal handling
ENTRYPOINT ["tini", "--"]

CMD ["node", "src/index.js"]
