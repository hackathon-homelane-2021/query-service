## Query Service

---
Try out at http://3110f098-732e-40d4-a72d-ce132266d51f.k8s.civo.com/
### Prerequisites
* Running data-service api url. 
* set data-service api url to env `DATA_API_URL`
* set `JWT_SECRET` env

### Instructions

    cd /query-service
    npm install

**run**
  
    npm start
>"Server listening at http://127.0.0.1:4444"


---
### API Spec. 
Refer [requests.http](requests.http) for examples

Get access-token by calling 

> POST http://localhost:4444/token
> { clientId: "client123" }


Get report

> GET http://localhost:4444/
> ?state=karnataka
> &date=2020/07/03
> headers: { "access-token": <token> }

