jest.genMockFromModule("axios");

const fetch = jest.fn(async (config) => {
  if (config.method === "error") {
    return Promise.resolve({
      statusText: "NOTOK",
    });
  }
  if (config.headers["access-token"] === "invalid") {
    return Promise.resolve({
      statusText: "NOTOK",
    });
  }
  return Promise.resolve({
    statusText: "OK",
    data: "done",
  });
});

fetch.defaults = {};
fetch.create = jest.fn();
fetch.responseListeners = [];
fetch.rejectionListeners = [];
fetch.interceptors = {
  request: {
    use: jest.fn(),
  },
  response: {
    use: jest.fn((resFn, rejFn) => {
      fetch.responseListeners.push(resFn);
      fetch.rejectionListeners.push(rejFn);
    }),
  },
};
fetch.request = fetch;

function Instance() {
  return fetch;
}

const create = Instance;

module.exports = {
  create,
  fetch,
};
