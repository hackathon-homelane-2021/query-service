const Controller = require("../controller");
const jwt = require("jsonwebtoken");
jest.mock("jsonwebtoken");

const log = {
  debug: jest.fn(),
};
describe("Controller Test", () => {
  let controller;
  let request;
  let response;

  beforeAll(() => {
    controller = Controller({
      config: {
        JWT_SECRET: "secret",
      },
    });
  });

  describe("getToken", () => {
    beforeEach(async () => {
      request = {
        body: {
          clientId: "clientId",
        },
        log,
      };
      jwt.sign.mockReturnValueOnce("token");
      response = await controller.getToken(request);
    });

    it("should call jwt.sign", function () {
      expect(jwt.sign).toHaveBeenCalledWith({ clientId: "clientId" }, "secret");
    });

    it("should return response", function () {
      expect(response).toEqual({ token: "token" });
    });
  });
});
