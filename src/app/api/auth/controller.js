const jwt = require("jsonwebtoken");

module.exports = (fastify) => {
  const { JWT_SECRET } = fastify.config;

  const getToken = async (request) => {
    const { clientId } = request.body;

    const token = jwt.sign(
      {
        clientId,
      },
      JWT_SECRET
    );

    return {
      token,
    };
  };

  return {
    getToken,
  };
};
