const Controller = require("./controller");
const Schema = require("./schema");

module.exports = (fastify, opt, done) => {
  const controller = Controller(fastify);

  fastify.route({
    method: "POST",
    url: "/token",
    schema: Schema.getToken,
    handler: controller.getToken,
  });

  done();
};
