const errorResponse = {
  type: "object",
  properties: {
    errors: {
      type: "array",
      items: {
        type: "object",
        properties: {
          id: {
            type: "string",
            description:
              "unique identifier for this particular occurrence of the problem",
          },
          code: {
            type: "string",
            description: `> an internal specific error code:
             - 1: Order Inactive.
             - 2: Broker exception.
             - 3: Validation error.
             `,
          },
          status: {
            type: "string",
            description: "the HTTP status code applicable to this problem",
          },
          message: {
            type: "string",
            description: "a short, human-readable summary of the problem",
          },
          detail: { type: "string" },
          meta: {
            type: "object",
            description:
              "a meta object containing non-standard meta-information",
          },
        },
        required: ["status"],
      },
    },
  },
  required: ["errors"],
};

const getToken = {
  description: "Generate access token",
  body: {
    type: "object",
    properties: {
      clientId: {
        type: "string",
      },
    },
    required: ["clientId"],
  },
  response: {
    200: {
      type: "object",
      properties: {
        token: { type: "string" },
      },
      required: ["token"],
    },
    "4xx": errorResponse,
    "5xx": errorResponse,
  },
};

module.exports = {
  getToken,
};
