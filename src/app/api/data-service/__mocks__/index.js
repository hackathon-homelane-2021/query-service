const service = jest.genMockFromModule("../index");

module.exports = () => {
  service.getAll = jest.fn();

  return service;
};
