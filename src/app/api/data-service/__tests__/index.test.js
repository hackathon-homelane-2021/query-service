const Service = require("../index");
const axios = require("axios");
jest.mock("axios");

describe("Data service test", () => {
  let request;
  let response;
  let fetchSpy;
  let token;
  let service = Service("url");

  beforeAll(() => {
    fetchSpy = axios.create();
  });

  describe("getAll", () => {
    describe("when success", () => {
      beforeEach(async () => {
        request = "query";
        token = "token";
        response = await service.getAll(request, token);
      });

      it("should call fetch", function () {
        expect(fetchSpy).toHaveBeenCalledWith({
          headers: {
            "access-token": "token",
          },
          method: "GET",
          params: "query",
          url: "report",
        });
      });

      it("should return response", function () {
        expect(response).toEqual("done");
      });
    });
    describe("when error", () => {
      beforeEach(async () => {
        request = "query";
        token = "invalid";
        try {
          response = await service.getAll(request, token);
        } catch (e) {
          response = e;
        }
      });

      it("should call fetch", function () {
        expect(fetchSpy).toHaveBeenCalledWith({
          headers: {
            "access-token": "invalid",
          },
          method: "GET",
          params: "query",
          url: "report",
        });
      });

      it("should return response", function () {
        expect(response).toEqual(new Error("data-service: fetch error"));
      });
    });
  });
});
