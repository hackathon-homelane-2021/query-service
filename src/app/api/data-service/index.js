const axios = require("axios");
const querystring = require("querystring");

module.exports = (baseURL, config = {}) => {
  const service = {};
  const logger = config.logger || require("../../logger");

  const fetch = axios.create({
    baseURL,
    timeout: config.timeout || 5000,
    paramsSerializer: function (params) {
      return querystring.stringify(params);
    },
  });

  service.getAll = async (query, token) => {
    const options = {
      method: "GET",
      url: "report",
      params: query,
      // Set auth header
      headers: {
        "access-token": token,
      },
    };
    logger.info(`data-service: ${baseURL}/${options.url}`);
    const res = await fetch.request(options);
    if (res.statusText === "OK") return res.data;

    logger.error({ err: res.data }, "data-service: fetch error");
    throw new Error("data-service: fetch error");
  };

  return service;
};
