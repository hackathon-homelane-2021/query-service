const Controller = require("../controller");
const DataService = require("../../data-service");
jest.mock("../../data-service");

const log = {
  debug: jest.fn(),
};
describe("Controller Test", () => {
  let controller;
  let request;
  let response;
  let dataService;

  beforeAll(() => {
    controller = Controller({
      config: {
        DATA_API_URL: "data-url",
      },
    });

    dataService = DataService();
  });

  describe("getAll", () => {
    beforeEach(async () => {
      request = {
        auth: {
          token: "token",
        },
        query: "query",
        log,
      };
      dataService.getAll.mockResolvedValueOnce("done");
      response = await controller.getAll(request);
    });

    it("should call data-service", function () {
      expect(dataService.getAll).toHaveBeenCalledWith("query", "token");
    });

    it("should return response", function () {
      expect(response).toEqual("done");
    });
  });
});
