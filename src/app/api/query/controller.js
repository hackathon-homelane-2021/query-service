const DataService = require("../data-service");

module.exports = (fastify) => {
  const { DATA_API_URL, TIMEOUT } = fastify.config;

  const dataService = DataService(DATA_API_URL, { timeout: TIMEOUT });

  /**
   * @param {Object} request http request object
   * @param {Object} request.query filter query.
   * @param {Array|String} request.query.state filter result by states.
   * @param {Array|String} request.query.date filter result by dates.
   */
  const getAll = async (request) => {
    const {
      auth: { token, clientId },
    } = request;
    request.log.debug(`calling service using clientId: ${clientId}`);
    return dataService.getAll(request.query, token);
  };

  return {
    getAll,
  };
};
