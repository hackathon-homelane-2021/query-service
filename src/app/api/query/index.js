const Controller = require("./controller");
const Schema = require("./schema");
const auth = require("../../../plugins/auth");

module.exports = (fastify, opt, done) => {
  const controller = Controller(fastify);
  fastify.register(auth);

  fastify.route({
    method: "GET",
    url: "/",
    schema: Schema.getAll,
    handler: controller.getAll,
  });

  done();
};
