const errorResponse = {
  type: "object",
  properties: {
    errors: {
      type: "array",
      items: {
        type: "object",
        properties: {
          id: {
            type: "string",
            description:
              "unique identifier for this particular occurrence of the problem",
          },
          code: {
            type: "string",
            description: `> an internal specific error code:
             - 1: Order Inactive.
             - 2: Broker exception.
             - 3: Validation error.
             `,
          },
          status: {
            type: "string",
            description: "the HTTP status code applicable to this problem",
          },
          message: {
            type: "string",
            description: "a short, human-readable summary of the problem",
          },
          detail: { type: "string" },
          meta: {
            type: "object",
            description:
              "a meta object containing non-standard meta-information",
          },
        },
        required: ["status"],
      },
    },
  },
  required: ["errors"],
};

const report = {
  type: "object",
  properties: {
    cases: {
      type: "array",
      items: {
        type: "object",
        additionalProperties: true,
      },
    },
    tests: {
      type: "array",
      items: {
        type: "object",
        additionalProperties: true,
      },
    },
    vaccines: {
      type: "array",
      items: {
        type: "object",
        additionalProperties: true,
      },
    },
  },
  required: [],
};

const getAll = {
  description: "Get report data",
  query: {
    type: "object",
    properties: {
      state: {
        type: ["string", "array"],
      },
      date: {
        type: "string",
      },
    },
    anyOf: [
      {
        required: ["state"],
      },
      {
        required: ["date"],
      },
    ],
  },
  response: {
    200: report,
    "4xx": errorResponse,
    "5xx": errorResponse,
  },
};

module.exports = {
  getAll,
};
