class ApiError extends Error {
  constructor({ id, status, code, message, meta }) {
    super(message);
    this.id = id;
    this.code = code;
    this.status = status;
    this.message = message;
    this.meta = meta;
  }
}

class Exception extends Error {
  constructor(message, code = "unknown", status) {
    super(message);
    this.code = code;
    this.status = status || "422";
  }
}

module.exports = {
  ApiError,
  Exception,
};
