module.exports = (fastify, opts, done) => {
  fastify.register(require("./api/auth"));
  fastify.register(require("./api/query"));
  done();
};
