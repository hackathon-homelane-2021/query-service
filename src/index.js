const Fastify = require("./server");
const routes = require("./app");

const start = async () => {
  const fastify = Fastify();
  try {
    fastify.register(routes);
    fastify.decorate("serviceAvailable", false);
    await fastify.ready();
    await fastify.listen(
      fastify.config.PORT || 3000,
      fastify.config.HOST || "0.0.0.0"
    );
    fastify.serviceAvailable = true;
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
