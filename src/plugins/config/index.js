const fastifyPlugin = require("fastify-plugin");
const fastifyEnv = require("fastify-env");

module.exports = fastifyPlugin((fastify, opts, next) => {
  const configSchema = {
    type: "object",
    properties: {
      NODE_ENV: {
        type: "string",
        default: "development",
      },
      HOST: {
        type: "string",
        default: "127.0.0.1",
      },
      PORT: {
        type: "integer",
        default: 4444,
      },
      JWT_SECRET: {
        type: "string",
      },
      DATA_API_URL: {
        type: "string",
      },
      TIMEOUT: {
        type: "integer",
        default: 10000,
      },
    },
    required: ["JWT_SECRET", "DATA_API_URL"],
  };

  fastify.register(fastifyEnv, {
    schema: configSchema,
    data: opts,
    dotenv: true,
  });

  next();
});
