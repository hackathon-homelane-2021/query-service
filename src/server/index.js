"use strict";

require("dotenv").config();
require("make-promises-safe");
const Fastify = require("fastify");
const config = require("../plugins/config");
const envSchema = require("env-schema");
const health = require("../plugins/health");

const errorHandler = require("../plugins/error-handler");

const schema = {
  type: "object",
  properties: {
    LOG_LEVEL: {
      type: "string",
      default: "debug",
    },
  },
};

const env = envSchema({ schema, dotenv: true });

function create(options = {}) {
  const fastify = Fastify({
    logger: {
      level: env.LOG_LEVEL,
      messageKey: "message",
      levelKey: "severity",
      useLevelLabels: true,
      // add custom serialiser to serialise istio tracing related properties
      serializers: {
        req(req) {
          return {
            method: req.method,
            url: req.url,
            hostname: req.hostname,
            path: req.path,
            parameters: req.parameters,
            id: req.id,
          };
        },
        ...((options.logger && options.logger.serializers) || {}),
      },
      ...options.logger,
    },
    disableRequestLogging: false,
  });

  fastify.register(config);
  fastify.register(errorHandler);
  fastify.register(health);

  return fastify;
}

module.exports = create;
